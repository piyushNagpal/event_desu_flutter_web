import 'package:flutter/material.dart';

//Following is the main color for app theme
// edwPrimaryColor means eventdesu web main Color
const edwPrimaryColor = Color(0xffFF914D);
//The text color to used
const edwTextColor = Color(0xff000000);
//The default border color to required widgets
const edwDefaultBorderColor = Color(0xffC4C4C4);
//The default icon color
const edwDefaultIconColor = Color(0xff9C9C9C);
//The default hyperlink txt color
const edwDefaultHyperLinkColor = Color(0xff7879F1);
//The default lable color
const edwDefaultLableColor = Color(0xff818181);
