import 'package:event_desu_flutter_web/size_config.dart';
import 'package:event_desu_flutter_web/theme.dart';
import 'package:flutter/material.dart';

import './routes.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Event Desu Flutter Web',
      debugShowCheckedModeBanner: false,
      routes: routes,
      theme: theme(),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter App'),
      ),
      body: const Center(
        child: Text('Widget Playground!'),
      ),
    );
  }
}
